FROM alpine:3.11.2

ARG KUBECTL_SOURCE=kubernetes-release/release

ARG KUBECTL_TRACK=stable.txt

ARG KUBECTL_ARCH=linux/amd64

RUN apk add --no-cache --update ca-certificates vim curl jq && \
    KOPS_URL=$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | jq -r ".assets[] | select(.name == \"kops-linux-amd64\") | .browser_download_url") && \
    curl -SsL --retry 5 "${KOPS_URL}" > /usr/local/bin/kops && \
    chmod +x /usr/local/bin/kops && \
    KUBECTL_VERSION=$(curl -SsL --retry 5 "https://storage.googleapis.com/${KUBECTL_SOURCE}/${KUBECTL_TRACK}") && \
    curl -SsL --retry 5 "https://storage.googleapis.com/${KUBECTL_SOURCE}/${KUBECTL_VERSION}/bin/${KUBECTL_ARCH}/kubectl" > /usr/local/bin/kubectl && \
    chmod +x /usr/local/bin/kubectl && \
    curl -LO https://get.helm.sh/helm-v3.0.2-linux-amd64.tar.gz && \
    tar -zxvf helm-v3.0.2-linux-amd64.tar.gz && \
    chmod +x linux-amd64/helm && \
    mv linux-amd64/helm /usr/local/bin/helm && \
    apk del jq